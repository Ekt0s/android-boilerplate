package io.louis_gabriel.android.boilerplate.ui.main;

import java.util.List;

import io.louis_gabriel.android.boilerplate.data.model.Ribot;
import io.louis_gabriel.android.boilerplate.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void showRibots(List<Ribot> ribots);

    void showRibotsEmpty();

    void showError();

}
