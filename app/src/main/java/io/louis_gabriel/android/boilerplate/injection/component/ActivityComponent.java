package io.louis_gabriel.android.boilerplate.injection.component;

import dagger.Subcomponent;
import io.louis_gabriel.android.boilerplate.injection.PerActivity;
import io.louis_gabriel.android.boilerplate.injection.module.ActivityModule;
import io.louis_gabriel.android.boilerplate.ui.main.MainActivity;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

}
