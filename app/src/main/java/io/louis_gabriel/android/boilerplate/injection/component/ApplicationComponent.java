package io.louis_gabriel.android.boilerplate.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import io.louis_gabriel.android.boilerplate.data.DataManager;
import io.louis_gabriel.android.boilerplate.data.SyncService;
import io.louis_gabriel.android.boilerplate.data.local.DatabaseHelper;
import io.louis_gabriel.android.boilerplate.data.local.PreferencesHelper;
import io.louis_gabriel.android.boilerplate.data.remote.RibotsService;
import io.louis_gabriel.android.boilerplate.injection.ApplicationContext;
import io.louis_gabriel.android.boilerplate.injection.module.ApplicationModule;
import io.louis_gabriel.android.boilerplate.util.RxEventBus;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);

    @ApplicationContext Context context();
    Application application();
    RibotsService ribotsService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    RxEventBus eventBus();

}
