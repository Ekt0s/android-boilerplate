package io.louis_gabriel.android.boilerplate.test.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import io.louis_gabriel.android.boilerplate.injection.component.ApplicationComponent;
import io.louis_gabriel.android.boilerplate.test.common.injection.module.ApplicationTestModule;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
